# midi2hamlib by Patrick EGLOFF
# started 15/02/17
# modif by Philippe Nouchi F6IFY
# started 3 Mars 2021
#
# Todo:       - Add commands for the FT817 (Yaesu old ones CAT commands)
#             - Add a windows to see Frequency and RIT
# v0.7 P. Nouchi - F6IFY le 17 Mars 2021
#       - Works for the TS590 @ F6BEE
# v0.6 P. Nouchi - F6IFY le 13 Mars 2021
#       - VFOs and RIT ok for the SunSDR
# v0.5 P. Nouchi - F6IFY le 13 Mars 2021
#       - correct bug to read the mode
# v0.4 P. Nouchi - F6IFY le 12 Mars 2021
#       - Bug correction for Icom TRX
# v0.3 P. Nouchi - F6IFY le 11 Mars 2021
#       Rename files and add mode of the kenwood radio
# v0.2 P. Nouchi - F6IFY le 10 Mars 2021
#       Add code for kenwood and Icom transceivers
# v0.1 P. Nouchi - F6IFY le 9 Mars 2021
#       Git on Bitbucket.org
#
# reads MIDI commands from a DJ console and sends commands to a Icom IC7610 via a COM port
# command includes the device number (0 to 4, depending on your setup),
# the MIDI status (depending on the kind of control),
# the control ID (depending on the device) and finally the control value
#
# import libraries
import configparser  # https://docs.python.org/3/library/configparser.html
import os
import struct
import sys
import math
import time
# import pygame              #http://www.pygame.org/docs/ref/midi.html
from tkinter import Menu

import pygame.midi
import serial  # https://pythonhosted.org/pyserial/
from serial import SerialException
from tkinter import *
import threading


# Config = configparser.ConfigParser()
Config = configparser.ConfigParser(
    allow_no_value=True)  # allow_no_value=True -> to allow adding comments without value, so no trailing =

INTRO = b"\xFE\xFE"
FROM_ADDR = b"\xE0"
SET_OPERATING_FREQ = b"\x00"
SET_OPERATING_MODE = b"\x06"
READ_OPERATING_FREQ = b"\x03"
READ_OPERATING_MODE = b"\x04"
EOM = b"\xFD"
postamble = b"\xFD"



########################################
# Read the ini file and get settings
# datas are strings so need to be converted !
########################################
def ReadIniFile():
    try:
        # declare global variables
        global RadioModel
        global TRXaddress
        global RadioSpeed
        global RadioPort
        global RadioBits
        global RadioStop
        global RadioParity
        global RadioXonXoff
        global RadioRtsCts
        global RadioDsrDtr
        global MidiDeviceIn
        global MidiDeviceOut
        global RadioDefaultMode
        global RadioDefaultVFO
        # global RadioFiltre
        global RadioMode
        global debug
        global kenwood
        global vfoStep
        global noAction
        global preamble
        global postamble
        global TRXaddress
        global IcomRITValue
        global RITStep
        global noAction
        global vfo
        global ritFreq

        # read ini file
        Config.read('midi2Cat.ini')

        # check if Midi section exists
        if Config.has_section('Midi'):
            MidiDeviceIn = Config.getint('Midi', 'deviceIN')  # get the device Midi IN, reads and converts in INT
            MidiDeviceOut = Config.getint('Midi', 'deviceOUT')  # get the device Midi OUT
        else:
            print("Midi section missing in config file. Please correct this !")
            sys.exit(1)

        # check if default section exists
        if Config.has_section('Default'):
            RadioDefaultMode = Config.get('Default', 'mode')
            RadioDefaultVFO = Config.get('Default', 'VFO')
            vfo = RadioDefaultVFO
        else:
            print("Default section missing in config file. Please correct this !")
            sys.exit(1)

        # check if Radio section exists
        if Config.has_section('Radio'):
            RadioModel = Config.get('Radio', 'model')
            if RadioModel == 'ic756pro3':
                preamble = b'\xfe\xfe\x6e\xe0'
                kenwood = False
                TRXaddress = b"\x6e"
            elif RadioModel == 'ic7610':
                preamble = b"\xFE\xFE\x98\xE0"  # ic7610
                TRXaddress = b"\x98"
                kenwood = False
            elif RadioModel == 'ts590':
                kenwood = True
            elif RadioModel == "kenwood":
                kenwood = True

            RadioPort = Config.get('Radio', 'comport')
            RadioSpeed = Config.getint('Radio', 'speed')  # reads and converts in INT
            RadioBits = Config.getint('Radio', 'bits')
            RadioStop = Config.getint('Radio', 'stop')
            RadioParity = Config.get('Radio', 'parity')
            RadioXonXoff = Config.getint('Radio', 'xonxoff')
            RadioRtsCts = Config.getint('Radio', 'rtscts')
            RadioDsrDtr = Config.getint('Radio', 'dsrdtr')
            print("Radio model", RadioModel, "Comport=", RadioPort, "Speed=", RadioSpeed, "Bits=", RadioBits, "Stop=",
                  RadioStop, "Parity=", RadioParity)
        else:
            print("Radio section missing in config file. Please correct this !")
            sys.exit(1)

        # check if Commands section exists
        if Config.has_section('Commands'):
            # print ("Commands section OK")
            # extraction des commandes avec la commande .partition('.')
            debug = Config.getint('Commands', 'debug')
            print('debug Value is %d' % debug)
        else:
            print("Commands section missing in config file. Please correct this !")
            sys.exit(1)

    # if an option is missing, raise an error
    except configparser.NoOptionError:
        print(
            "Missing option(s) in config file !\nPlease correct this or remove file to allow creating a default one !")
        sys.exit(1)


#    except configparser.NoSectionError:
#        print ("Missing section(s) in config file !\nPlease correct this or remove file to allow creating a default one !")
#        sys.exit(1)


########################################
# create_midi2Cat_inifile
# in case the ini file does not exist, create one with default values
########################################
def CreateIniFile():
    inifile = open("midi2Cat.ini", 'w')

    # add default section
    Config.add_section('Default')
    Config.set('Default', '# midi2Cat config file')
    Config.set('Default', '# defaults values created by program')
    Config.set('Default', 'mode', 'USB')
    Config.set('Default', 'VFO', 'A')
    # add section Midi
    Config.add_section('Midi')
    # add settings
    Config.set('Midi', 'deviceIN', '1')
    Config.set('Midi', 'deviceOUT', '3')

    # add section Radio
    Config.add_section('Radio')
    # add settings
    Config.set('Radio', 'model', 'ic7610')
    Config.set('Radio', 'comport', 'COM2')
    Config.set('Radio', 'speed', '19200')
    Config.set('Radio', 'bits', '8')
    Config.set('Radio', 'stop', '1')
    Config.set('Radio', 'parity', 'N')
    Config.set('Radio', 'xonxoff', '0')
    Config.set('Radio', 'rtscts', '0')
    Config.set('Radio', 'dsrdtr', '0')

    # add section Commands
    Config.add_section('Commands')
    # add settings
    Config.set('Commands', '144.1', 'TS1;')
    Config.set('Commands', '14412', 'TS0;')
    Config.set('Commands', '144.3', 'FR0;FT1;')

    # and lets write it out...
    Config.write(inifile)
    inifile.close()

    # now read the config file
    ReadIniFile()


########################################
# send data to radio
########################################
def SendToRadio(strCat):
    if kenwood:
        RadioSer.write(strCat.encode())
    else:
        RadioSer.write(strCat)


def SendString(string, length):  # Write a string list
    count = 0
    reply = ""
    while (count < length):
        senddata = int(bytes(string[count], 'UTF-8'), 16)
        reply = RadioSer.write(struct.pack('>B', senddata))
        count += 1


########################################
# get mode from radio
########################################
def GetModeFromRadiokenwood():
    mode = 'unknown'
    strCat = 'MD;'
    SendToRadio(strCat)

    while mode == 'unknown':
        codeReceived = RadioSer.read(3)
        if codeReceived == b'':
            SendToRadio(strCat)
        elif codeReceived == b'ID;':
            SendToRadio('ID020;')
        elif codeReceived == b'MD1':
            mode = 'LSB'
        elif codeReceived == b'MD2':
            mode = 'USB'
        elif codeReceived == b'MD3':
            mode = 'CW'
        elif codeReceived == b'MD4':
            mode = 'FM'
        elif codeReceived == b'MD5':
            mode = 'AM'
        elif codeReceived == b'MD6':
            mode = 'FSK'
        elif codeReceived == b'MD7':
            mode = 'CWR'
        else:
            mode = 'unknown'
    print('Mode is ' + mode)
    return mode


########################################


def GetModeFromRadio():
    if kenwood:
        GetModeFromRadiokenwood()
    else:
        strCat = b"\xfe\xfe\x98\xe0\x04\xFD"
        RadioSer.write(strCat)
    filtre = b''
    intFiltre = 0
    intMode = 0
    DataStream = b''
    data = 0
    i = 0
    while DataStream == b'' and i < 10:
        i += 1
        DataStream = RadioSer.read(6)
        if len(DataStream) > 2:
            data = DataStream[2]
        if data != 0xe0:
            DataStream = b''
        else:
            intMode = DataStream[5]
            filtre = RadioSer.read(1)
            intFiltre = filtre[0]
        print('.', end='')
    if debug >= 2: print("data is %s" % DataStream + "mode is %d" % intMode)

    modeReceived = ""

    if intMode == 3:  # CW
        modeReceived = "CW"
    elif intMode == 4:  # RTTY
        modeReceived = "RTTY"
    elif intMode == 0:  # LSB
        modeReceived = "LSB"
    elif intMode == 1:  # USB
        modeReceived = "USB"

    if intFiltre == 0 or intFiltre > 3:
        print("retry please")
    else:
        print("mode is " + modeReceived + " and filtre is FIL%d" % intFiltre)
    return [modeReceived, intFiltre]


########################################
# Send Mode and filtre number to radio
########################################


########################################
# Set frequency
########################################
def SetFrequencykenwood(dwFrequency, vfo):
    f = str("%011d" % dwFrequency)
    strCat = 'F' + vfo + f + ';'
    if debug >= 1:
        print('Command Sent is ' + strCat)
    SendToRadio(strCat)


########################################
# change radio mode and filtre number
########################################
def ChangeModekenwood(mode):
    global RadioMode
    if mode == 'CW':
        strCat = 'MD3;MD;'  # MODE = CW
        RadioMode = 'CW'
        # switch on backlight
        midi_out.write([[[144, 49, 127], 0], [[144, 50, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0]])
    elif mode == 'LSB':
        strCat = 'MD1;MD;'
        RadioMode = 'SSB'
        # switch on backlight
        midi_out.write([[[144, 51, 127], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 52, 0], 0]])
    elif mode == 'USB':
        strCat = 'MD2;MD;'
        RadioMode = 'SSB'
        # switch on backlight
        midi_out.write([[[144, 51, 0], 0], [[144, 49, 0], 0], [[144, 50, 127], 0], [[144, 52, 0], 0]])
    elif mode == 'FSK':
        strCat = 'MD6;MD;'
        RadioMode = 'FSK'
        # switch on backlight
        midi_out.write([[[144, 52, 127], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 51, 0], 0]])
    SendToRadio(strCat)


def ChangeMode(mode, filtre):
    if filtre == 1:
        if mode == 'CW':
            strCat = b"\xFE\xFE\x98\xE0\x06\x03\x02\xFD"  # Increment filtre
        elif mode == 'USB':
            strCat = b"\xFE\xFE\x98\xE0\x06\x01\x02\xFD"
        elif mode == 'LSB':
            strCat = b"\xFE\xFE\x98\xE0\x06\x00\x02\xFD"
        else:
            strCat = b"\xFE\xFE\x98\xE0\x06\x04\x02\xFD"
    elif filtre == 2:
        if mode == 'CW':
            strCat = b"\xFE\xFE\x98\xE0\x06\x03\x03\xFD"  # Increment filtre
        elif mode == 'USB':
            strCat = b"\xFE\xFE\x98\xE0\x06\x01\x03\xFD"
        elif mode == 'LSB':
            strCat = b"\xFE\xFE\x98\xE0\x06\x00\x03\xFD"
        else:
            strCat = b"\xFE\xFE\x98\xE0\x06\x04\x03\xFD"
    else:
        if mode == 'CW':
            strCat = b"\xFE\xFE\x98\xE0\x06\x03\x01\xFD"  # Increment filtre
        elif mode == 'USB':
            strCat = b"\xFE\xFE\x98\xE0\x06\x01\x01\xFD"
        elif mode == 'LSB':
            strCat = b"\xFE\xFE\x98\xE0\x06\x00\x01\xFD"
        else:
            strCat = b"\xFE\xFE\x98\xE0\x06\x04\x01\xFD"

    # RadioSer.write(strCat)
    SendToRadio(strCat)
    print("Now Mode sent is " + mode + " and filtre sent is FIL" + str(strCat[6]))


########################################
# send CIV Transceive
########################################
def SendCIVTransceive(on):
    if kenwood:
        pass
    else:
        if on:
            strCat = b"\xFE\xFE\x98\xE0\x1A\x05\x42\x01\xFD"  # CI-V Transceive ON
        else:
            strCat = b"\xFE\xFE\x98\xE0\x1A\x05\x42\x00\xFD"  # CI-V Transceive Off
        RadioSer.write(strCat)


########################################
# put radio on VFO A
########################################
def ChangeVFO(vfo):
    if vfo == 'A':
        if kenwood:
            strCat = 'FR0;'
        else:
            strCat = b"\xfe\xfe\x98\xE0\x07\xd0\xfd"
        # switch on backlight
        midi_out.write([[[144, 35, 127], 0], [[144, 33, 0], 0], [[144, 3, 0], 0], [[144, 4, 0], 0]])
    elif vfo == 'B':
        if kenwood:
            strCat = 'FR1;'
        else:
            strCat = b"\xfe\xfe\x98\xE0\x07\xd1\xfd"
        # switch on backlight
        midi_out.write([[[144, 33, 127], 0], [[144, 35, 0], 0], [[144, 3, 0], 0], [[144, 4, 0], 0]])
    SendToRadio(strCat)


########################################
# put RIT OFF
########################################
def PutRITOFF():
    if kenwood:
        RadioSer.write(b'RT0;')
    else:
        RadioSer.write(b"\xfe\xfe\x98\xE0\x21\x01\x00\xFD")
    midi_out.write([[[144, 83, 127], 0], [[144, 82, 0], 0], [[144, 81, 0], 0]])


########################################
# put RIT ON
########################################
def PutRITON():
    if kenwood:
        RadioSer.write(b'RT1;')
    else:
        RadioSer.write(b"\xfe\xfe\x98\xE0\x21\x01\x01\xFD")
    midi_out.write([[[144, 81, 127], 0], [[144, 82, 0], 0], [[144, 83, 0], 0]])


########################################
# Clear RIT
# ########################################
def ClearRIT():
    if kenwood:
        RadioSer.write(b'RC;')
    else:
        RadioSer.write(b"\xfe\xfe\x98\xE0\x21\x00\x00\x00\x00\xFD")
        IcomRITValue = 0
    midi_out.write([[[144, 82, 127], 0]])  # CUE-B backlight ON


def setRIT(ritFreq, Up):
    f = str("%05d" % abs(ritFreq))
    if Up:
        strCat = 'RU' + f + ';'  # RIT offset > 0
    else:
        strCat = 'RD' + f + ';'  # RIT offset < 0
    if debug >= 1:
        print('Command Sent is ' + strCat)
    SendToRadio(strCat)


########################################
# start of program
########################################
# check if ini file exists and read the settings
inifile = 'midi2Cat.ini'
if os.path.isfile(inifile):
    ReadIniFile()
else:
    print("Configuration file does not exist, creating it")
    CreateIniFile()

#######################################
# define the serial port for radio
#######################################
try:
    RadioSer = serial.Serial(
        # set the port parameters
        port=RadioPort,
        baudrate=RadioSpeed,
        bytesize=RadioBits,
        stopbits=RadioStop,
        parity=RadioParity,
        xonxoff=RadioXonXoff,  # software flow control
        rtscts=RadioRtsCts,  # hardware (RTS/CTS) flow control
        dsrdtr=RadioDsrDtr,  # hardware (DSR/DTR) flow control
        timeout=0.1)

    # open radio port
    # test if it is open
    ok = RadioSer.is_open
    print('Is the Com open %s ' % ok)
# RadioSer.close()

# if COM port communication problem
except SerialException:
    print("No communication to", RadioPort, ", check configuration file and available ports on computer !")
    sys.exit(1)


#####################################
# init the Midi device
#####################################
def InitMidiDevice(MidiDeviceIn):
    pygame.midi.init()

    # testing if device is already opened
    if pygame.midi.get_device_info(MidiDeviceIn)[4] == 1:
        print('Error: Can''t open the MIDI device - It''s already opened!')
        sys.exit(1)

    # testing if input device is an input
    if pygame.midi.get_device_info(MidiDeviceIn)[3] == 1:
        print('Error: Midi input device selected in not an input ! Please correct this.')
        sys.exit(1)

    # testing if input device is an input
    if pygame.midi.get_device_info(MidiDeviceOut)[3] == 0:
        print('Error: Midi output device selected in not an output ! Please correct this')
        sys.exit(1)

        # count the number of Midi devices connected
    MidiDeviceCount = pygame.midi.get_count()
    print("Number of Midi devices found =", MidiDeviceCount)

    # display the default Midi device
    print("\nWindows default Midi input device =", pygame.midi.get_default_input_id())
    print("Windows default Midi output device =", pygame.midi.get_default_output_id())

    # display Midi device selected by config file
    print("\nConfig file selected Midi INPUT device =", MidiDeviceIn)
    print("Config file selected Midi OUTPUT device =", MidiDeviceOut)

    # display infos about the selected Midi device
    print("\nInfos about Midi devices :")
    for n in range(pygame.midi.get_count()):
        print(n, pygame.midi.get_device_info(n))

    # open Midi device
    # my_input = pygame.midi.Input(MidiDeviceIn)


###########################################
# all backlights on Midi console
###########################################
def ledsON():
    midi_out.write([[[144, 1, 127], 0], [[144, 2, 127], 0], [[144, 3, 127], 0], [[144, 4, 127], 0], [[144, 33, 127], 0],
                    [[144, 34, 127], 0], [[144, 35, 127], 0], [[144, 49, 127], 0], [[144, 50, 127], 0],
                    [[144, 51, 127], 0], [[144, 52, 127], 0], [[144, 81, 127], 0], [[144, 82, 127], 0],
                    [[144, 83, 127], 0], [[144, 43, 127], 0], [[144, 45, 127], 0], [[144, 48, 127], 0]])


###########################################
# all backlights on Midi console
###########################################
def ledsOFF():
    midi_out.write(
        [[[144, 1, 0], 0], [[144, 2, 0], 0], [[144, 3, 0], 0], [[144, 4, 0], 0], [[144, 33, 0], 0], [[144, 34, 0], 0],
         [[144, 35, 0], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0],
         [[144, 81, 0], 0], [[144, 82, 0], 0], [[144, 83, 0], 0], [[144, 43, 0], 0], [[144, 45, 0], 0],
         [[144, 48, 0], 0]])


###########################################
# all backlights on Midi console
###########################################
def blinkLED(numTimes, speed):
    for i in range(0, numTimes):  ## Run loop numTimes
        ledsON()
        time.sleep(speed)
        ledsOFF()
        time.sleep(speed)


###########################################
# send datas to Midi console
###########################################
def sendOutput(output_device):
    pass


#################################
# Increment or Decrement the VFO of kenwood TRX
#################################
def UpKenwood(RadioSer, vfoStep, vfo):
    if vfo == 'A':
        currentVFO = b'FA'
        strCat = 'FA;'
    else:
        currentVFO = b'FB'
        strCat = 'FB;'
    SendToRadio(strCat)
    fr = b''
    restToRead = 1
    RadioSer.flush()
    while fr != currentVFO and restToRead != 0:
        fr = RadioSer.read(2)
        restToRead = RadioSer.inWaiting()
        # print(restToRead)
    if fr == currentVFO:
        fre = RadioSer.read(11)
        if debug >= 2: print(fre)
        freq = int(fre)
        freq += vfoStep
        SetFrequencykenwood(freq, vfo)


def DownKenwood(RadioSer, vfoStep, vfo):
    if vfo == 'A':
        currentVFO = b'FA'
        strCat = 'FA;'
    else:
        currentVFO = b'FB'
        strCat = 'FB;'
    SendToRadio(strCat)
    fr = b''
    restToRead = 1
    RadioSer.flush()
    while fr != currentVFO and restToRead != 0:
        fr = RadioSer.read(2)
        restToRead = RadioSer.inWaiting()
    if fr == currentVFO:
        fre = RadioSer.read(11)
        freq = int(fre)
        freq -= vfoStep
        SetFrequencykenwood(freq, vfo)


def GetfreqKenwood(RadioSer, vfo):
    if vfo == 'A':
        currentVFO = b'FA'
        strCat = 'FA;'
    else:
        currentVFO = b'FB'
        strCat = 'FB;'
    SendToRadio(strCat)
    fr = b''
    restToRead = 1
    RadioSer.flush()
    while fr != currentVFO and restToRead != 0:
        fr = RadioSer.read(2)
        restToRead = RadioSer.inWaiting()
        # print(restToRead)
    if fr == currentVFO:
        fre = RadioSer.read(11)
        # print(fre)
        freq = int(fre)
        return freq


#################################
# Increment or Decrement the VFO of Icom TRX
#################################
def DownIcom(RadioSer, vfoStep):
    frequency = 0
    while frequency == 0 or frequency is None:
        frequency = getFrequency(RadioSer)
        if frequency < 1800000:
            frequency = 0
    if debug>= 2: print(str(frequency / 1000))
    fToSend = frequency - vfoStep
    setFrequency(RadioSer, fToSend)


def UpIcom(RadioSer, vfoStep):
    frequency = 0
    while frequency == 0 or frequency is None:
        frequency = getFrequency(RadioSer)
        if frequency < 1800000:
            frequency = 0
    if debug >= 2: print(str(frequency / 1000))
    fToSend = frequency + vfoStep
    setFrequency(RadioSer, fToSend)


########################################
# main loop read Midi, convert and send to radio
########################################
# http://stackoverflow.com/questions/15768066/reading-piano-notes-on-python/24821493#24821493
def readInput(input_device):
    # global DualWatchIsON
    DualWatchIsON = 0
    vfoStep = 100
    noAction = 0
    ACTION = 5
    IcomRITValue = 0
    RITStep = 10
    vfo = 'A'
    ritFreq = 0
    while True:
        if input_device.poll():
            event = input_device.read(1)[0]
            if debug >= 2: print(event)
            data = event[0]
            # print (data)
            # timestamp = event[1]
            device = data[0]
            status = data[1]
            control = data[2]
            value = data[3]
            # for debugging
            # print (device)
            # print(status)
            # print (control)
            # print (value)
            modeReceived = ""
            # detect rotation of jogs and pots
            if device == 176:
                if RadioModel == 'ts590':
                    if status == 48:
                        if noAction < ACTION:  # 1 Action on 20 increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control < 64:  # VFO UP
                                UpKenwood(RadioSer, vfoStep, vfo)
                            else:  # VFO Down
                                DownKenwood(RadioSer, vfoStep, vfo)
                    elif status == 49:
                        if noAction < ACTION:  # 1 Action on xx increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control > 64:
                                ritFreq -= RITStep
                                RadioSer.write(b'RD00010;')  # RIT DOWN
                            else:
                                ritFreq += RITStep
                                RadioSer.write(b'RU00010;')  # RIT UP
                            print('RIT is %5d' % ritFreq)
                    elif status == 54:  # SLIDER
                        strCat = format("AG%04d;" % (control))  # RX VOLUME
                        SendToRadio(strCat)
                    elif status == 60 and RadioMode == 'CW':  # CW bandwidth
                        print("%04d" % (math.floor(100 + (1000 - 100) * control / 127)))
                        strCat = format("FW%04d;" % (math.floor(100 + (1000 - 100) * control / 127)))
                        SendToRadio(strCat)
                    elif status == 60 and RadioMode == 'FSK':
                        print("%04d" % (math.floor(250 + (1500 - 250) * control / 127)))
                        strCat = format("FW%04d;" % (math.floor(250 + (1500 - 250) * control / 127)))
                        SendToRadio(strCat)
                elif kenwood:
                    if status == 49:
                        if noAction < ACTION:  # 1 Action on xx increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control > 64:
                                ritFreq -= RITStep
                                if ritFreq >= 0:
                                    ritDirection = True
                                else:
                                    ritDirection = False

                                # RadioSer.write(b'RD00010;')  # RIT DOWN
                            else:
                                ritFreq += RITStep
                                if ritFreq >= 0:
                                    ritDirection = True
                                else:
                                    ritDirection = False
                            setRIT(ritFreq, ritDirection)
                                # RadioSer.write(b'RU00010;')  # RIT UP
                            print('RIT is %5d' % ritFreq)
                    elif status == 48:
                        if noAction < ACTION:  # 1 Action on 20 increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control < 64:  # VFO UP
                                UpKenwood(RadioSer, vfoStep, vfo)
                            else:  # VFO Down
                                DownKenwood(RadioSer, vfoStep, vfo)
                elif status == 49:  # Now Icom JoG B
                    if control > 64:  # RIT Down
                        IcomRITValue += RITStep
                        strCat = (b'\xfe\xfe\x98\xe0\x21\x00')
                        RITBytes = IcomRITValue.to_bytes(3, 'little')
                        RITBytes = RITBytes.join(b'\xfd')
                        RadioSer.write(strCat.join(RITBytes))  # RIT DOWN
                    else:
                        RadioSer.write(b'RU;')  # RIT UP

                elif status == 48:  # Jog A for Icom
                    if noAction < 5:
                        noAction += 1
                    elif control > 64:  # Down
                        DownIcom(RadioSer, vfoStep)
                    else:
                        UpIcom(RadioSer, vfoStep)

            # detects buttons
            if device == 144:
                if status == 1 and control == 127:  # Down 10 Hz
                    if kenwood:
                        DownKenwood(RadioSer, vfoStep, vfo)

                    else:
                        DownIcom(RadioSer, vfoStep)

                    # midi_out.write([[[144, 1, 127], 0]])
                    # midi_out.write([[[144, 1, 0], 0]])
                elif status == 2 and control == 127:  # Read Frequency
                    if kenwood:
                        UpKenwood(RadioSer, vfoStep, vfo)
                    else:
                        UpIcom(RadioSer, vfoStep)
                elif status == 3:
                    if kenwood:
                        if control > 64:  # PTT On
                            strCat = 'TX0;TX0;'
                        else:
                            strCat = 'RX;'  # PTT Off
                    else:
                        if control > 64:  # PTT On
                            strCat = b"\xfe\xfe\x98\xE0\x1c\x00\x01\xfd"
                        else:
                            strCat = b"\xfe\xfe\x98\xE0\x1c\x00\x00\xfd"  # PTT Off
                    SendToRadio(strCat)
                    # midi_out.write([[[144, 3, 127], 0], [[144, 4, 0], 0], [[144, 34, 127], 0], [[144, 35, 127], 0]])
                elif status == 4 and control == 127:  # Read Frequency
                    filtrenew = GetModeFromRadio()[1]
                    if filtrenew < 3:
                        filtrenew += 1
                    else:
                        filtrenew = 1
                    print("New filtre is %2X" % filtrenew)

                elif status == 48 and control == 127:  # Ask mode
                    if kenwood:
                        modeReceived = GetModeFromRadiokenwood()
                    else:
                        strCat = b"\xfe\xfe\x98\xE0\x04\xfd"
                        SendToRadio(strCat)
                        modeReceived = GetModeFromRadio()[0]
                    # print(modeReceived)
                    # midi_out.write([[[144, 4, 127], 0], [[144, 3, 0], 0], [[144, 34, 127], 0], [[144, 35, 127], 0]])

                elif status == 35 and control == 127:  # VFO A
                    vfo = 'A'
                    ChangeVFO(vfo)
                    if not kenwood:
                        SendCIVTransceive(True)
                elif status == 33 and control == 127:  # VFO B
                    vfo = 'B'
                    ChangeVFO(vfo)
                elif status == 34:  # CUE key pressed
                    if control == 127:  # A = B
                        if kenwood:
                            # RadioSer.write(b'AB;') ne fonctionne pas sur SunSDR
                            freqA = 0
                            while freqA == 0 or freqA == None:
                                freqA = GetfreqKenwood(RadioSer, 'A')
                            SetFrequencykenwood(freqA, 'B')
                        else:
                            RadioSer.write(b"\xfe\xfe\x98\xE0\x07\xb1\xfd")
                elif status == 43 and control == 127:  # if REC key pressed
                    if kenwood:
                        pass
                    else:
                        if DualWatchIsON == 1:  # if radio is already ON
                            strCat = b"\xFE\xFE\x98\xE0\x07\xc0\xfd"  # send DualWatch OFF to radio
                            SendToRadio(strCat)
                            ledsOFF()  # switch OF Fall backlights
                            DualWatchIsON = 0  # set flag OFF
                        elif DualWatchIsON == 0:  # if radio is OFF
                            strCat = b"\xFE\xFE\x98\xE0\x07\xc1\xfd"  # send DualWatch ON
                            SendToRadio(strCat)
                            midi_out.write([[[144, 43, 127], 0]])  # SWITCH ON BACKLIGHT LED
                            # switch radio to default mode
                            # ChangeMode(RadioDefaultMode)
                            # put radio to default VFO
                            ChangeVFO(RadioDefaultVFO)
                            # switch OFF RIT
                            PutRITOFF()
                            DualWatchIsON = 1
                elif status == 45 and control == 127:  # Automix key pressed
                    if vfoStep == 10:  # Change Step in Jog A (VFO)
                        vfoStep = 100
                    else:
                        vfoStep = 10
                elif status == 49 and control == 127:
                    mode = 'CW'
                    if kenwood:
                        ChangeModekenwood(mode)
                    else:
                        RadioFiltre = 0
                        while RadioFiltre == 0 or RadioFiltre > 3:
                            RadioFiltre = GetModeFromRadio()[1]
                        # time.sleep(1)
                        ChangeMode(mode, RadioFiltre)
                    # setMode(RadioSer, mode)
                    # switch on backlight
                    midi_out.write([[[144, 49, 127], 0], [[144, 50, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0]])
                elif status == 50 and control == 127:
                    mode = 'USB'
                    if kenwood:
                        ChangeModekenwood(mode)
                    else:
                        RadioFiltre = 0
                        while RadioFiltre == 0 or RadioFiltre > 3:
                            RadioFiltre = GetModeFromRadio()[1]
                        # time.sleep(1)
                        ChangeMode(mode, RadioFiltre)
                    midi_out.write([[[144, 50, 127], 0], [[144, 49, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0]])
                elif status == 51 and control == 127:  # MODE = USB
                    mode = 'LSB'
                    if kenwood:
                        ChangeModekenwood(mode)
                    else:
                        RadioFiltre = 0
                        while RadioFiltre == 0 or RadioFiltre > 3:
                            RadioFiltre = GetModeFromRadio()[1]
                        # time.sleep(1)
                        ChangeMode(mode, RadioFiltre)
                    midi_out.write([[[144, 51, 127], 0], [[144, 50, 0], 0], [[144, 49, 0], 0], [[144, 52, 0], 0]])
                elif status == 52 and control == 127:
                    mode = 'FSK'
                    if kenwood:
                        ChangeModekenwood(mode)
                    else:
                        RadioFiltre = 0
                        while RadioFiltre == 0 or RadioFiltre > 3:
                            RadioFiltre = GetModeFromRadio()[1]
                        # time.sleep(1)
                        ChangeMode(mode, RadioFiltre)
                    # switch on backlight
                    midi_out.write([[[144, 52, 127], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 51, 0], 0]])

                elif status == 81 and control == 127:
                    PutRITON()
                elif status == 83 and control == 127:
                    PutRITOFF()
                elif status == 82 and control == 127:
                    ClearRIT()
                    ritFreq = 0


# non-reversed
def bcd4(d1, d2, d3, d4):
    out = b''
    first = bytes([(16 * d1) + d2])
    second = bytes([(16 * d3) + d4])
    # return 16 * d1 + d2, 16 * d3 + d4
    out = out.join([first, second])
    return out


# pack 2 BCD digits
def bcd2(d1, d2):
    out = b''
    only = bytes([(16 * d1) + d2])
    out = out.join([only])
    return out


def expectChar(byte, expected):
    """Return true if we got what we expected"""
    if byte == expected:
        # print("good %02x" % (ord(expected))),
        return True
    else:
        #         print("wanted %02x unexpected %02x" % (ord(expected), ord(byte))),
        return False


def setFrequency(ser, freq):
    fs = "%010d" % freq
    out = b''
    out1 = bcd4(int(fs[8]), int(fs[9]), int(fs[6]), int(fs[7]))
    out2 = bcd4(int(fs[4]), int(fs[5]), int(fs[2]), int(fs[3]))
    out3 = b'\x00'  # 'bcd2(int(fs[0]), int(fs[1]))
    out = out.join([out1, out2, out3])

    strCat = b'\xfe\xfe\x98\xE0\x00'
    strCattoSend = b''
    strCattoSend = strCattoSend.join([strCat, out, b'\xfd'])
    if debug >= 2: print(strCattoSend)
    RadioSer.write(strCattoSend)

    echo = RadioSer.read(6)
    if debug >= 2: print(f"got reply of {len(echo):d} chars")


'''
def setMode(Radioser, mode):
    sendStr = (INTRO + TO_ADDR + FROM_ADDR + SET_OPERATING_MODE)
    if mode == "LSB":
        sendStr += "\x00"
    elif mode == "USB":
        sendStr += "\x01"
    elif mode == "AM":
        sendStr += "\x02"
    elif mode == "CW":
        sendStr += "\x03"
    sendStr += EOM
    SendString(sendStr, len(sendStr))

    echo = Radioser.read(len(sendStr))
    # print("got reply of %d chars" % len(echo))
'''


def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result


'''
def getFreq(RadioSer):
    strCat = b'\xfe\xfe\x98\xE0\x03\xfd'
    RadioSer.write(strCat)

    ByteReceived = b''
    DataStream = []
    Value = []
    while RadioSer.inWaiting() != 0 and (ByteReceived != b'\xfd' or len(DataStream) <= 11):
        ByteReceived = RadioSer.read()
        if ByteReceived == b'\xfe':
            DataStream = [b'\xfe', b'\xfe']
        elif ByteReceived == b'\xfd' and len(DataStream) <= 6:  # it is the echo
            DataStream = []
        elif ByteReceived != b'':
            DataStream.append(ByteReceived)
    print(DataStream)
    print("it was DataStream received")
    for k in [10, 11, 8, 9, 6, 7, 4, 5, 2, 3]:
        Value.append(bytes_to_int(DataStream[k]))

    print(Value)'''


def getFrequency(RadioSer):
    if kenwood:
        if vfo == 'A':
            currentVFO = b'FA'
            strCat = 'FA;'
        else:
            currentVFO = b'FB'
            strCat = 'FB;'
        SendToRadio(strCat)
        fr = b''
        restToRead = 1
        RadioSer.flush()
        while fr != currentVFO:  # and restToRead != 0:
            fr = RadioSer.read(2)
            restToRead = RadioSer.inWaiting()
            # print(restToRead)
        if fr == currentVFO:
            fre = RadioSer.read(11)
            if debug >= 2: print(fre)
            freq = int(fre)
    else:
        strCat = b'\xfe\xfe\x98\xE0\x03\xfd'
        RadioSer.write(strCat)
        '''
        DataStream = b''
        data = 0
        freqa = b''
        freqb = b''
        freqc = b''
        freqd = b''
        freqe = b''
        endOfStream = b''
        strFrequency = ''
        i = 0
        while DataStream == b'' and i < 10:
            i += 1
            DataStream = RadioSer.read(6)
            if len(DataStream) > 2:
                data = DataStream[2]
            if data != 0xe0:
                DataStream = b''
            else:
                freqa = DataStream[5]
                freqb = RadioSer.read(1)
                freqc = RadioSer.read(1)
                freqd = RadioSer.read(1)
                freqe = RadioSer.read(1)
                endOfStream = RadioSer.read(1)

            print('.', end='')
        if endOfStream != b'\xfd':
            return 0

        f = ord(freqe) * 100000000 + ord(freqd) * 1000000 + ord(freqc) * 10000 + ord(freqb) * 100 + freqa
        #strFrequency = ("%02x" %ord(freqe) + "%02x" % ord(freqd) + "%02x" % ord(freqc) + "%02x" % ord(freqb) + "%02x" % freqa)
        #strfre = "%02x%02x%02x%02x" % int(strFrequency)
        #print(str(freq) + ' Hz')
        #if debug: print("%x" %freqa + "%x" % ord(freqb) + "%x" % ord(freqc) + "%x" % ord(freqd) + "%x" % ord(freqe))
        print("Band is %2x Mhz" % ord(freqd) + "Frequency is %d" %f)
        print(f)
        return f
        '''

        echo = RadioSer.read(len(strCat))
        # print("got reply of %d chars" % len(echo))

        if not expectChar(RadioSer.read(), b"\xfe"): return 0
        if not expectChar(RadioSer.read(), b"\xfe"): return 0
        if not expectChar(RadioSer.read(), b"\xe0"): return 0
        if not expectChar(RadioSer.read(), b"\x98"): return 0

        byte = b""
        result = ""
        while byte != b"\xfd" and len(result) < 11:
            byte = RadioSer.read()
            if byte == b'\xfd' and len(result) <= 6:  # it is the echo
                result = ""
            elif byte != b'':
                result += str("%02x" % ord(byte))
                # print("%02x" % ord(byte))

        # print(result)
        res = 0
        try:
            if len(result) == 12:
                f = 0
                fr = []
                # freq = []
                # for k in [10, 11, 8, 9, 6, 7, 4, 5, 2, 3]:

                for k in [8, 9, 6, 7, 4, 5, 2, 3]:
                    res = int(result[k])
                    fr.append(res)
                    # freq += result[k]

                # print('freq is ' + str(freq))
                # print('fr is ' + str(fr))
                f = fr[0] * 10000000 + fr[1] * 1000000 + fr[2] * 100000 + fr[3] * 10000 + fr[4] * 1000 + fr[5] * 100 + \
                    fr[6] * 10 + fr[7]
            else:
                f = 0
        except:
            f = 0
        return f


global midi_out



'''
def programDJControl():
    # if __name__ == '__main__':
    # pygame.midi.init()
    InitMidiDevice(MidiDeviceIn)
    my_input = pygame.midi.Input(MidiDeviceIn)  # open Midi device
    midi_out = pygame.midi.Output(MidiDeviceOut)

    # blink all backlights as a test
    blinkLED(2, 0.3)

    # start reading Midi input in a loop
    readInput(my_input)
'''
def afficheur():
    screen_vfo = Tk()
    screen_vfo.title("VFO A & B")
    screen_vfo.geometry("450x250")
    screen_vfo.minsize(330, 160)
    frameA = Frame(screen_vfo, bg='#4065A4', bd=1, relief=SUNKEN)
    frameB = Frame(screen_vfo, bg='#4065A4', bd=1, relief=SUNKEN)

    screen_vfo.config(background='#4065A4')
    label_vfoA = Label(frameA, text="14254.254", font=("courrier", 50), bg='#4065A4', fg='yellow')
    label_vfoA.pack(expand=YES)
    label_vfoB = Label(frameB, text="14054.132", font=("courrier", 50), bg='#4065A4', fg='yellow')
    label_vfoB.pack(expand=YES)

    frameA.pack(expand=YES)
    frameB.pack(expand=YES)

    # create menu
    menu_bar = Menu(screen_vfo)
    file_menu: Menu = Menu(menu_bar, tearoff=0)
    file_menu.add_command(label="Start")
    file_menu.add_command(label="Quit", command=screen_vfo.quit)
    menu_bar.add_cascade(label="File", menu=file_menu)
    screen_vfo.config(menu=menu_bar)

    screen_vfo.mainloop()
######################
# Let's start !
######################
if __name__ == '__main__':
    InitMidiDevice(MidiDeviceIn)
    my_input = pygame.midi.Input(MidiDeviceIn)  # open Midi device
    midi_out = pygame.midi.Output(MidiDeviceOut)
    # blink all backlights as a test
    blinkLED(2, 0.3)
    # afficheur()
    '''
    th1 = threading.Thread(target=readInput(my_input))
    th2 = threading.Thread(target=afficheur())

    th1.start()
    th2.start()
    th1.join()
    th2.join()
    '''
    readInput(my_input)
    print('Fin du Programme')

